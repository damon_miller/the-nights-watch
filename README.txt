Group Project for CPSC 310: Introduction to Software Engineering

In our design, we set up the Carta class to be the central “client” object, initialized by the 
EntryPoint interface. This is based off of our knowledge of the StockWatcher tutorial and UML 
diagram, where the same EntryPoint interface is used to create a starting point for the 
initialization of the program. Also, much like StockWatcher, Carta calls on the ClickHandler 
and ClickEvent classes to manage user interaction with our program.

The remaining packages are based off of our own understanding of the project’s functional 
requirements and Priority 1 tasks in our initial Sprint. After Carta is initialized, it 
accesses the ArtDatabase class, which contains the data of all known public artworks in the 
city of Vancouver (received from parsing the KML files and City of Vancouver webpages using our 
KMLParser and URLParser classes) with each individual artwork being an ArtPiece object. These 
objects can be searched, filtered, and sorted by Carta through the ArtDatabase. They can also 
be rated by UserAccounts through the Rating object – each art piece can have zero to many 
ratings, while each account can make at most one rating to each individual artwork – and the 
ratings are stored in the user account. The UserDatabase collects and manages all user 
accounts, including an AdminAccount which can update the ArtDatabase with new artwork data. 
Carta can also access the SocialMedia class in order to enable social media functionality. 
Lastly, the data is visualized using the UI package where a map and table display all necessary 
artwork information (they do not interact with the ArtDatabase to prevent unnecessary coupling; 
Carta gets all relevant info and passes it to the Map and Table objects), as well as a Login 
object to allow users to access their account.