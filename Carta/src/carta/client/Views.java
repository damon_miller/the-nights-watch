package carta.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.objectify.annotation.Index;

public class Views implements Serializable {	
	Map<String, Integer> userViews = new HashMap<String, Integer>();

	@Index Integer totalNumberOfViews;
	
	void addView(String userId) {
		Integer userNumberOfViews = userViews.get(userId);
		
		if (userNumberOfViews == null)
			userNumberOfViews = 1;
		else
			++userNumberOfViews;
		
		userViews.put(userId, userNumberOfViews);
		++totalNumberOfViews;
	}	
}
