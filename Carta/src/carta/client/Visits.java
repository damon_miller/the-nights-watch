package carta.client;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.googlecode.objectify.annotation.Index;

public class Visits implements Serializable {
	@Index Set<String> users = new HashSet<String>();
	
	public boolean markAsVisited(String userId) {
		return users.add(userId);
	}
	
	public void markAsUnvisited(String userId) {
		users.remove(userId);
	}
	
	public boolean hasVisited(String userId) {
		return users.contains(userId);
	}
	
	public Integer getNumberOfVisits() {
		return users.size();
	}
}
