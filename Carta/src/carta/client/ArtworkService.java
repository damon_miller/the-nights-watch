package carta.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import carta.server.Stock;

@RemoteServiceRelativePath("artwork")
public interface ArtworkService extends RemoteService {
  public String updateArtworks_int(int maximumNumberOfArtworks) throws NotLoggedInException;
  public String updateArtworks(int maximumNumberOfArtworks) throws NotLoggedInException;
  public List<Artwork> getArtworks(int maximumNumberOfArtworks) throws NotLoggedInException;
  public String lastUpdated() throws NotLoggedInException;
  public void saveArtwork(Artwork Artwork) throws NotLoggedInException;
  public List<Artwork> getArtworks(String oqs) throws NotLoggedInException;
}