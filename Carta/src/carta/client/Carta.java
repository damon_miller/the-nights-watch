package carta.client;

//import com.gargoylesoftware.htmlunit.javascript.host.geo.Geolocation;
import carta.client.SortableLabel;

import com.gargoylesoftware.htmlunit.javascript.host.geo.Geolocation;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabBar.Tab;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalScrollbar;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;

import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.core.client.GWT;

import java.util.List;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.maps.client.InfoWindowContent;
import com.google.gwt.maps.client.MapUIOptions;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.Maps;
import com.google.gwt.maps.client.control.LargeMapControl;
import com.google.gwt.maps.client.event.MarkerClickHandler;
import com.google.gwt.maps.client.geocode.DirectionQueryOptions;
import com.google.gwt.maps.client.geocode.DirectionResults;
import com.google.gwt.maps.client.geocode.Directions;
import com.google.gwt.maps.client.geocode.DirectionsCallback;
import com.google.gwt.maps.client.geocode.DirectionsPanel;
import com.google.gwt.maps.client.geocode.Placemark;
import com.google.gwt.maps.client.geocode.Route;
import com.google.gwt.maps.client.geocode.StatusCodes;
import com.google.gwt.maps.client.geocode.Waypoint;
import com.google.gwt.maps.client.geom.LatLng;
import com.google.gwt.maps.client.overlay.Marker;
//import com.google.gwt.user.client.ui.


public class Carta implements EntryPoint {

	final int DEFAULT_NUMBER_OF_ARTWORKS_TO_SAVE = 14;
	final int DEFAULT_NUMBER_OF_ARTWORKS_TO_LOAD = 50;

	private VerticalPanel leftScreenVerticalPanel = new VerticalPanel();

	private final ArtworkServiceAsync artworkService = GWT.create(ArtworkService.class);

	private LoginInfo loginInfo = null;
	private VerticalPanel loginPanel = new VerticalPanel();
	private Label loginLabel = new Label("Please sign in to your Google Account to access Carta.");
	private Anchor signInLink = new Anchor("Sign In");
	private Anchor signOutLink = new Anchor("Sign Out");

	private Button skipLogin = new Button("Skip Login");
	MapWidget mainMap;

	final String LOCAL_BROWSER = "http://127.0.0.1:8888/Carta.html?gwt.codesvr=127.0.0.1:9997";

	public void onModuleLoad() {
		// Check login status using login service.
		LoginServiceAsync loginService = GWT.create(LoginService.class);

		// for debugging, redirect to local browser
		String hostPageUrl;
		if ( ! GWT.isProdMode() )
			hostPageUrl = LOCAL_BROWSER;
		else
			hostPageUrl = GWT.getHostPageBaseURL();

		// load login screen
		loginService.login(hostPageUrl, new AsyncCallback<LoginInfo>() {
			public void onFailure(Throwable error) {
				handleError(error);
			}

			public void onSuccess(LoginInfo result) {
				loginInfo = result;
				if (loginInfo.isLoggedIn()) {
					if (loginInfo.isAdmin) {
						loadAdminUserScreen();
					}
					else {
						loadNormalUserScreen();								
					}
				} else {
					loadLogin();
				}
			}
		});
	}

	private void loadLogin() {
		// Assemble login panel.
		signInLink.setHref(loginInfo.getLoginUrl());

		// add handler to call when skip login is clicked
		skipLogin.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				loadNormalUserScreen();
			}
		});

		loginPanel.add(loginLabel);
		loginPanel.add(signInLink);
		loginPanel.add(skipLogin);

		RootPanel.get("loginpanel").add(loginPanel);
	}


	//-- Admin user page --//
	Label lastUpdatedLabel = new Label("");

	final NumberOfArtworksField userRequestField = new NumberOfArtworksField(DEFAULT_NUMBER_OF_ARTWORKS_TO_LOAD);
	
	void loadAdminUserScreen() {
		
		final NumberOfArtworksField requestField = new NumberOfArtworksField(DEFAULT_NUMBER_OF_ARTWORKS_TO_SAVE);
		final TextArea logger = new TextArea();

		// Make a new button that does something when you click it.
		final Button updateButton = new Button("Update Artwork Database");
		updateButton.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				updateButton.setEnabled(false);
				Utility.appendLineToTextArea(logger, "Updating...");

				artworkService.updateArtworks(
						requestField.getNumberOfArtworks(), new AsyncCallback<String>() {

							public void onFailure(Throwable error) {
								handleError(error);
							}

							public void onSuccess(String result) {
								//String test = logger.getText() + result + " artworks updated.\n";
								//logger.setText(test);
								Utility.appendLineToTextArea(logger, result);

								refreshWhenLastUpdated();

								updateButton.setEnabled(true);
							}
						});
			}
		});


		Button goToUserPageButton = new Button("Go To User Screen");
		goToUserPageButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				rootPanel.clear();
				loadNormalUserScreen();
			}
		});		

		refreshWhenLastUpdated();	


		// Set up sign out hyperlink.
		signOutLink.setHref(loginInfo.getLogoutUrl());		

		// Layout
		VerticalPanel adminVerticalPanel = new VerticalPanel();
		adminVerticalPanel.add(requestField);
		adminVerticalPanel.add(updateButton);
		adminVerticalPanel.add(goToUserPageButton);
		adminVerticalPanel.add(logger);
		adminVerticalPanel.add(lastUpdatedLabel);
		adminVerticalPanel.add(signOutLink);

		// Associate the Main panel with the HTML host page.
		rootPanel.add(adminVerticalPanel);
	}

	void refreshWhenLastUpdated() {
		artworkService.lastUpdated(new AsyncCallback<String>() {
			public void onFailure(Throwable error) {

				handleError(error);
			}

			public void onSuccess(String dateTimeLastUpdated) {
				String notification = "Database last updated: " + dateTimeLastUpdated;

				lastUpdatedLabel.setText(notification);
			}
		});	
	}	
	//-- --//


	final RootPanel rootPanel = RootPanel.get("artworkList").get();

	FlexTable[] tables = {
			new FlexTable(),
			new FlexTable(),
			new FlexTable()		
			};
	
	ListBox[] listOfThemes = {
			new ListBox(),
			new ListBox(),
			new ListBox()
			};
	


	TabPanel tp;

	//-- Normal user page --//
	private void loadNormalUserScreen() {

		Maps.loadMapsApi("AIzaSyATLV2UUcy-aPRxRpFm6cubRi6ov-pxdY4", "2", false, new Runnable() {
			public void run() {
				// for debugging
				//String result = Maps.getVersion();

				loadMap();
				
				// for debugging
				/*Artwork artwork = new Artwork();
				final double startingLatitude = 49.258670;
				final double startingLongitude = -123.126783;
				Coordinates coor = new Coordinates(startingLongitude, startingLatitude, 0);
				artwork.coordinates = coor;
				loadDirections(artwork);*/
				
				loadTabPanel();
			}
		});
	}

	void loadMap() {

		// Open a map centered on Metrotown
		final double startingLat = 49.258670;
		final double startingLongitude = -123.126783;
		LatLng startingCoordinates = LatLng.newInstance(startingLat, startingLongitude);

		/*final int zoomLevel = 12;
		mainMap = new MapWidget(startingCoordinates, zoomLevel);
		mainMap.setSize("770px","480px");
		
		
		// Add some controls for the zoom level
		mainMap.addControl(new LargeMapControl());
			
		DecoratorPanel decorator = new DecoratorPanel();
		decorator.add(mainMap);
		//decorator.add(dp);

		// Add the map to the HTML host page
		RootPanel.get("hm-map").add(decorator);*/
		
		

		// adapted from "SimpleDirectionsDemo.java" of HelloMaps
		// https://github.com/emanoeltadeu/Hello
		final Grid grid = new Grid(1, 2);
		grid.setWidth("100%");
		grid.getCellFormatter().setWidth(0, 0, "500px");
		grid.getCellFormatter().setVerticalAlignment(0, 0,
				HasVerticalAlignment.ALIGN_TOP);
		grid.getCellFormatter().setWidth(0, 1, "300px");
		grid.getCellFormatter().setVerticalAlignment(0, 1,
				HasVerticalAlignment.ALIGN_TOP);
		//mainMap = new MapWidget(LatLng.newInstance(42.351505, -71.094455), 15);
		mainMap = new MapWidget(startingCoordinates, 12);
		mainMap.setHeight("480px");
		grid.setWidget(0, 0, mainMap);
		directionsPanel = new DirectionsPanel();
		ScrollPanel vpDp = new ScrollPanel();
		vpDp.setHeight("450px");
		vpDp.add(directionsPanel);
		grid.setWidget(0, 1, vpDp);
		directionsPanel.setSize("100%", "100%");
		
		DecoratorPanel dp1 = new DecoratorPanel();
		dp1.add(grid);
		
		RootPanel.get("hm-map").add(dp1);
	}

	DirectionsPanel directionsPanel;
	DirectionResults directionResults = null;
	boolean isLoading = false;

	void loadDirections(final Artwork artwork) {

		// get current location
		com.google.gwt.geolocation.client.Geolocation geolocation = 
				com.google.gwt.geolocation.client.Geolocation.getIfSupported();
		
		
		if (geolocation != null) {
			geolocation.getCurrentPosition(new Callback<Position, PositionError>() {

				@Override
				public void onFailure(PositionError reason) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onSuccess(Position result) {
					// TODO Auto-generated method stub
					LatLng currentLatLng = 
							LatLng.newInstance(
									result.getCoordinates().getLatitude(), 
									result.getCoordinates().getLongitude());


					//result.getCoordinates()

					HorizontalPanel hp = new HorizontalPanel();

					//				 final LatLng ATLANTA = LatLng.newInstance(33.7814790,
					//				      -84.3880580);
					// Open a map centered on metrotown
					/*final double startingLat = 49.258670;
					final double startingLongitude = -123.126783;
					LatLng ATLANTA = LatLng.newInstance(startingLat, startingLongitude);*/
					final double startingLat = artwork.coordinates.latitude;
					final double startingLongitude = artwork.coordinates.longitude;
					final LatLng artworkLatLng = LatLng.newInstance(startingLat, startingLongitude);

					/*   mainMap = new MapWidget(ATLANTA, 15);
			    //map.setSize("400px", "480px");
			    mainMap.setUIToDefault();
			    mainMap.getElement().getStyle().setPropertyPx("margin", 15);
			    hp.add(mainMap);
			    DirectionsPanel directionsPanel = new DirectionsPanel();
			    hp.add(directionsPanel);
			    directionsPanel.setSize("100%", "100%");

//			    //initWidget(hp);

				RootPanel.get("hm-map").add(hp);

			    //List<Waypoint> waypoints = new ArrayList<Waypoint>();
			    //Waypoint wp1 = new Waypoint(result2);
			    //waypoints.add(wp1);
			    //waypoints.add(new Waypoint(ATLANTA));
			   	Waypoint[] waypoints = new Waypoint[2];
			    Waypoint wp1 = new Waypoint(result2);
			    waypoints[0] = wp1;
			    waypoints[1] = new Waypoint(ATLANTA);

			    DirectionQueryOptions opts = new DirectionQueryOptions(mainMap,
			        directionsPanel);

			    // Create directions from Midtown Atlanta to the Airport with a *few*
			    // stops along the way.
			    Directions.loadFromWaypoints(waypoints, opts, new DirectionsCallback() {

			      public void onFailure(int statusCode) {
			        Window.alert("Failed to load directions: Status "
			            + StatusCodes.getName(statusCode) + " " + statusCode);
			      }

			      public void onSuccess(DirectionResults result) {
			        GWT.log("Successfully loaded directions.", null);

			        // A little exercise of the route API
			        List<Route> routes = result.getRoutes();
			        for (Route r : routes) {
			          Placemark start = r.getStartGeocode();
			          GWT.log("start of route: " + start.getAddress(), null);
			          Placemark end = r.getEndGeocode();
			          GWT.log("end of route: " + end.getAddress(), null);
			        }
			      }
			    });*/

					//   initWidget(grid);
					Waypoint[] waypoints = new Waypoint[2];
				    waypoints[0] = new Waypoint(currentLatLng);
				    waypoints[1] = new Waypoint(artworkLatLng);
				    
				    DirectionQueryOptions opts = new DirectionQueryOptions(mainMap, directionsPanel);
				    //String query = "from: 500 Memorial Dr, Cambridge, MA to: 4 Yawkey Way, Boston, MA";
				    //Directions.load(query, opts, new DirectionsCallback() {
				    
				    Directions.loadFromWaypoints(waypoints, opts, new DirectionsCallback() {

				      public void onFailure(int statusCode) {
				        Window.alert("Failed to load directions: Status "
				            + StatusCodes.getName(statusCode) + " " + statusCode);
				      }

				      public void onSuccess(DirectionResults result) {
				    	  //haveDirections = true;
				    	  directionResults = result;
				    	  
				        GWT.log("Successfully loaded directions.", null);
				        
				        //synchronized (lock) {
				        //}
				        	
				        	mainMap.setCenter(artworkLatLng);
							mainMap.panTo(artworkLatLng);
							mainMap.setZoomLevel(17);
							mainMap.getInfoWindow().open(mainMap.getCenter(),new InfoWindowContent(artwork.description));
							
				        	isLoading = false;
				      }
				    });

				}
			});
		}
	}

	void loadTabPanel() {

		// initialize cache
		artworksCache = new ArrayList<List<Artwork>>(NUMBER_OF_TABS);
		for (int i = 0; i < NUMBER_OF_TABS; i++)
			artworksCache.add(new ArrayList<Artwork>());

		FlexTable artworksTable = tables[ARTWORKS_TAB];
		FlexTable favouriteArtworksTable = tables[FAVOURITES_TAB];
		FlexTable visitedTable = tables[VISITED_TAB];

		artworksTable.setStyleName("artworksTable");
		favouriteArtworksTable.setStyleName("artworksTable");
		visitedTable.setStyleName("artworksTable");

		tp = new TabPanel();
		tp.setWidth("500px");
		tp.setHeight("480px");


		addTab(tp, ARTWORKS_TAB, artworksTable, "All artworks");
		addTab(tp, FAVOURITES_TAB, favouriteArtworksTable, "Favourites");
		addTab(tp, VISITED_TAB, visitedTable, "Visited");

		
		/*tp.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			
			@Override
			public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {

				  int tabId = event.getItem();
				 //refreshThemes(listOfThemes[tabId]);
				 
				  //for (ListBox lb : listOfThemes)
//					  refreshThemes(lb);
				   
				   //List<Artwork> artworks = artworksCache.get(tabId);
				   
				   //displayArtworks(tables[tabId], artworks, tabId);
				  
				  MyRunnable myRunnable = new MyRunnable(10);
			        Thread t = new Thread(myRunnable)
			        t.start();
		}
		});*/

		tp.addSelectionHandler(new SelectionHandler<Integer>() {
			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				final int tabId = event.getSelectedItem();

				// refreshThemes(listOfThemes[tabId]);
				//List<Artwork> list = artworksCache.get(tabId);
				//displayArtworks(tables[tabId], list, tabId);
				//MyRunnable myRunnable = new MyRunnable(tabId);
				//Thread t = new Thread(myRunnable);
				//t.start();
				//refreshThemes(listOfThemes[tabId]);
				// http://stackoverflow.com/questions/2590850/threading-in-gwt-client
				/*Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					   public void execute() {
					   //   .. code here is executed using the timer technique.
						   refreshThemes(listOfThemes[tabId]);
					   }
					});
				*/
				
				refreshThemes(listOfThemes[tabId]);
			}
		});
		tp.selectTab(ARTWORKS_TAB);

		leftScreenVerticalPanel.add(tp);



		artworkService.lastUpdated(new AsyncCallback<String>() {
			public void onFailure(Throwable error) {

				handleError(error);
			}

			public void onSuccess(String dateTimeLastUpdated) {
				String notification = "Database last updated: " + dateTimeLastUpdated;
				lastUpdatedLabel.setText(notification);

			}
		});
		leftScreenVerticalPanel.add(userRequestField);
		
		userRequestField.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				// http://stackoverflow.com/a/9397697
				int tabIndex = tp.getTabBar().getSelectedTab();

				refreshThemes(listOfThemes[tabIndex]);
			}
		});

		
		leftScreenVerticalPanel.add(lastUpdatedLabel);

		if (loginInfo.isLoggedIn()) {
			signOutLink.setHref(loginInfo.getLogoutUrl());
			leftScreenVerticalPanel.add(signOutLink);
		}
		else {
			loginPanel.remove(skipLogin);
		}

		// Associate the Main panel with the HTML host page.
		RootPanel.get("artworkList").add(leftScreenVerticalPanel);
	}

	// http://stackoverflow.com/questions/3489543/how-to-call-a-method-with-a-separate-thread-in-java
	class MyRunnable implements Runnable {

	    private int var;

	    public MyRunnable(int var) {
	        this.var = var;
	    }

	    public void run() {
	        // code in the other thread, can reference "var" variable
	    	refreshThemes(listOfThemes[var]);
	    }
	}
	
	
	final int ARTWORKS_TAB = 0;
	final int FAVOURITES_TAB = 1;
	final int VISITED_TAB = 2;
	final int NUMBER_OF_TABS = +1 + VISITED_TAB;

	//void getArt

	final int SETTINGS_COLUMN = 0;
	final int TITLE_COLUMN = 1;
	final int ARTIST_COLUMN = 2;
	final int YEAR_COLUMN = 4;
	final int UPVOTES_COLUMN = 5;
	final int DOWNVOTES_COLUMN = 6;
	final int VISITED_COLUMN = 7;

	final int SHUFFLE = 0;
	final int MOST_LIKED = 1;
	final int MOST_VIEWED = 2;
	final int NEWER = 3;
	final int OLDER = 4;

	//VerticalPanel initializeTab(int tabIndex, final FlexTable artworksTableInstance) {
	void addTab(TabPanel tabPanel, final int tabIndex,  final FlexTable table, String title) {
		int ri = table.getRowCount();

		//Label plPanel = new Label("PLACEHOLDER");
		//final NumberOfArtworksField requestField = new NumberOfArtworksField(DEFAULT_NUMBER_OF_ARTWORKS);

		// Make a new list box, adding a few items to it.
		//final ListBox themes = new ListBox();
		final ListBox themes = listOfThemes[tabIndex];
		
		themes.addItem("Shuffle");
		themes.addItem("Most Liked Artworks");
		themes.addItem("Most Viewed");
		//themes.addItem("Newer Artworks");
		//themes.addItem("Older Artworks");
		// Make enough room for all five items (setting this value to 1 turns it
		// into a drop-down list).
		themes.setVisibleItemCount(0);

		//final String maxNumberOfArtworks = "limit:" + requestField.getNumberOfArtworks();
		final String minNumberOfUpvotes = "filter:ratings.numberOfUpvotes >,0";
		final String minNumberOfViews = "filter:views.totalNumberOfViews >,0";
		final String descendByUpvotes = "order:-ratings.numberOfUpvotes";
		final String descendByViews = "order:-views.totalNumberOfViews";
		//final String descendByYear = "order:-yearInstalled";
		//final String ascendByYear = "order:yearInstalled";
		
		/*String[][] queries = {
			{ "filter:ratings.numberOfUpvotes >,0"
			, "filter:views.totalNumberOfViews >,0"
			, "order:-ratings.numberOfUpvotes"
			, "order:-yearInstalled"
			, "order:yearInstalled"
			},
			
			{ "filter:ratings.numberOfUpvotes >,0"
			, "filter:views.totalNumberOfViews >,0"
			, "order:-ratings.numberOfUpvotes"
			, "order:-yearInstalled"
			, "order:yearInstalled"
			},
				
			{ "filter:ratings.numberOfUpvotes >,0"
			, "filter:views.totalNumberOfViews >,0"
			, "order:-ratings.numberOfUpvotes"
			, "order:-yearInstalled"
			, "order:yearInstalled"
			}
		};*/
		

		String userId = loginInfo.userId;
		final String baseShuffleQuery = "";
		final String baseMostLikedQuery = minNumberOfUpvotes + ";" + descendByUpvotes;
		final String baseMostViewedQuery = minNumberOfViews + ";" + descendByViews;
		//final String baseNewerQuery = descendByYear;
		//final String baseOlderQuery = ascendByYear;
		
		//final String noVisits = "filter:visits.users!,"+userId;
		final String noVisits = "";
		final String mainShuffle = baseShuffleQuery+";"+noVisits;
		final String mainMostLikedQuery = noVisits+";"+baseMostLikedQuery;
		final String mainMostViewedQuery = baseMostViewedQuery+";"+noVisits;
		//final String mainNewerQuery = noVisits+";"+baseNewerQuery;
		//final String mainOlderQuery = noVisits+";"+baseOlderQuery;
		
		final String usersFavourites = "filter:ratings.userRatings." + userId + ",1";  // http://stackoverflow.com/questions/25437341/gae-w-objectify-can-you-query-a-hashmap
		final String favouritesShuffleQuery = baseShuffleQuery+";"+usersFavourites;
		final String favouritesMostLikedQuery = baseMostLikedQuery+";"+usersFavourites;;
		final String favouritesMostViewedQuery = baseMostViewedQuery+";"+usersFavourites;;
		//final String favouritesNewerQuery = usersFavourites+";"+baseNewerQuery;
		//final String favouritesOlderQuery = usersFavourites+";"+baseOlderQuery;
		
		final String yesVisits = "filter:visits.users," + userId;
		final String visitsShuffle = baseShuffleQuery+";"+yesVisits;
		final String visitsMostLikedQuery = baseMostLikedQuery+";"+yesVisits;;
		final String visitsMostViewedQuery = baseMostViewedQuery+";"+yesVisits;;
		//final String visitsNewerQuery = yesVisits+";"+baseNewerQuery;
		//final String visitsOlderQuery = yesVisits+";"+baseOlderQuery;

		//final String visitedQuery = "filter:visits.users." + userId + " !=,null";  // http://stackoverflow.com/questions/25437341/gae-w-objectify-can-you-query-a-hashmap
		
/*		final String favouritesShuffleQuery = usersFavourites+";"+shuffleQuery;
		final String favouritesMostLikedQuery = usersFavourites+";"+mostLikedQuery;
		final String favouritesMostViewedQuery = usersFavourites+";"+mostViewedQuery;
		final String favouritesNewerQuery = usersFavourites+";"+newerQuery;
		final String favouritesOlderQuery = usersFavourites+";"+olderQuery;
*/
		switch (tabIndex) {

		case ARTWORKS_TAB:
			themes.addChangeHandler(new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					int themeIndex = themes.getSelectedIndex();

					String query = null;

					switch (themeIndex) {
					case SHUFFLE: {
						query = mainShuffle;
						break;
					}
					case MOST_LIKED: {
						query = mainMostLikedQuery;
						break;
					}
					case MOST_VIEWED: {
						query = mainMostViewedQuery;
						break;
					}
					/*case NEWER: {
						query = mainNewerQuery;
						break;
					}
					case OLDER: {
						query = mainOlderQuery;
						break;
					}*/
					}

					query += ";" + "limit:" + userRequestField.getNumberOfArtworks();
					queryCache[tabIndex] = query;
					getArtworks(table, query, tabIndex);
					//displayArtworks(table, artworksCache.get(tabIndex), tabIndex);
				}
			});
			break;

		case FAVOURITES_TAB:
			themes.addChangeHandler(new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					int themeIndex = themes.getSelectedIndex();

					String query = null;

					switch (themeIndex) {
					case SHUFFLE: {
						query = favouritesShuffleQuery;
						break;
					}
					case MOST_LIKED: {
						query = favouritesMostLikedQuery;
						break;
					}
					case MOST_VIEWED: {
						query = favouritesMostViewedQuery;
						break;
					}
					/*case NEWER: {
						query = favouritesNewerQuery;
						break;
					}
					case OLDER: {
						query = favouritesOlderQuery;
						break;
					}*/
					}

					query += ";" + "limit:" + userRequestField.getNumberOfArtworks();
					queryCache[tabIndex] = query;
					getArtworks(table, query, tabIndex);  
					//displayArtworks(table, artworksCache.get(tabIndex), tabIndex);
				}
			});
			break;

		case VISITED_TAB:
			themes.addChangeHandler(new ChangeHandler() {

				@Override
				public void onChange(ChangeEvent event) {
					int themeIndex = themes.getSelectedIndex();

					String query = null;

					switch (themeIndex) {
					case SHUFFLE: {
						query = visitsShuffle;
						break;
					}
					case MOST_LIKED: {
						query = visitsMostLikedQuery;
						break;
					}
					case MOST_VIEWED: {
						query = visitsMostViewedQuery;
						break;
					}
					/*case NEWER: {
						query = visitsNewerQuery;
						break;
					}
					case OLDER: {
						query = visitsOlderQuery;
						break;
					}*/
					}

					query += ";" + "limit:" + userRequestField.getNumberOfArtworks();
					queryCache[tabIndex] = query;
					getArtworks(table, query, tabIndex);  
					//displayArtworks(table, artworksCache.get(tabIndex), tabIndex);
				}
			});
			break;
		}




		/*requestField.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				refreshThemes(themes);
			}
		});*/

		themes.setItemSelected(SHUFFLE, true);
		//refreshThemes(themes);

		VerticalPanel vp2 = new VerticalPanel();
		//vp2.add(requestField);
		vp2.add(themes);

		//-- create sortable labels --//
		final SortableLabelGroup slf = new SortableLabelGroup();
		
		final SortableLabel titleLabel = slf.createLabel("Title", SortableLabel.SORT_ASCEND);
		titleLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(titleLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.title.compareTo(o2.title);
					}
				});
			}
		});

		
		final SortableLabel artistLabel = slf.createLabel("Artist", SortableLabel.SORT_ASCEND);
		artistLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(artistLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.artist.compareTo(o2.artist);
					}
				});
			}
		});
		
		
		final SortableLabel yearLabel = slf.createLabel("Year", SortableLabel.SORT_ASCEND);
		yearLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(yearLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.yearInstalled.compareTo(o2.yearInstalled);
					}
				});
			}
		});
		
		
		final SortableLabel upvotesLabel = slf.createLabel("Upvotes", SortableLabel.SORT_DESCEND);
		upvotesLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(upvotesLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.ratings.numberOfUpvotes - o2.ratings.numberOfUpvotes;
					}
				});
			}
		});
		
		
		final SortableLabel downvotesLabel = slf.createLabel("Downvotes", SortableLabel.SORT_DESCEND);
		downvotesLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(downvotesLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.ratings.numberOfDownvotes - o2.ratings.numberOfDownvotes;
					}
				});
			}
		});
		
		
		final SortableLabel visitedLabel = slf.createLabel("Visited", SortableLabel.SORT_DESCEND);
		visitedLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				addSortLogic(visitedLabel, tabIndex, table, new Comparator<Artwork>() {
					@Override
					public int compare(Artwork o1, Artwork o2) {
						return o1.visits.getNumberOfVisits() - o2.visits.getNumberOfVisits();
					}
				});
			}
		});
		
		// -- //

		FlexTable headingsTable = new FlexTable();		
		headingsTable.setWidget(ri, SETTINGS_COLUMN, vp2);
		headingsTable.setWidget(ri, TITLE_COLUMN, titleLabel);
		headingsTable.setWidget(ri, ARTIST_COLUMN, artistLabel);		
		headingsTable.setWidget(ri, YEAR_COLUMN, yearLabel);
		headingsTable.setWidget(ri, UPVOTES_COLUMN, upvotesLabel);
		headingsTable.setWidget(ri, DOWNVOTES_COLUMN, downvotesLabel);		
		headingsTable.setWidget(ri, VISITED_COLUMN, visitedLabel);	

		//verticalScrollPanel = new ScrollPanel();
		ScrollPanel verticalScrollPanel = scrollPanels[tabIndex] = new ScrollPanel();
		verticalScrollPanel.add(table);
		verticalScrollPanel.setHeight("450px");
		verticalScrollPanel.setWidth("500px");

		// assemble
		VerticalPanel vp = new VerticalPanel();
		vp.add(headingsTable);
		vp.add(verticalScrollPanel);



		tabPanel.add(vp, title);
		int lastTabIndex = -1 + tabPanel.getTabBar().getTabCount();
		Tab tab = tabPanel.getTabBar().getTab(lastTabIndex);
		

		/*tab.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				refreshThemes(themes);
				slf.reset();
			}
		});*/
	}
	
	ScrollPanel[] scrollPanels = new ScrollPanel[NUMBER_OF_TABS];

	void refreshThemes(ListBox themes) {
		DomEvent.fireNativeEvent(Document.get().createChangeEvent(), themes);
		//System.out.println("");
	}
	
	/*void addSortLogic (SortableLabel sl, String field, int tabIndex, FlexTable table) {
		String sortQuery = null;
		
		if (sl.toggle() == SortableLabel.SORT_ASCEND) {
			sortQuery = "order:" + field + ";" + queryCache[tabIndex];
			//sortQuery = queryCache[tabIndex] + ";" + "order:" + field;
			//displayArtworks(table, artworkscache, tabIndex);
			List<Artwork> list = artworksCache.get(tabIndex);
			Collections.sort(list, new Comparator<Artwork>() {

				@Override
				public int compare(Artwork o1, Artwork o2) {
					// TODO Auto-generated method stub
					return 0;
				}
			});
			
		}
		else {
			sortQuery = "order:-" + field + ";" + queryCache[tabIndex];
			//sortQuery = queryCache[tabIndex] + ";" + "order:-" + field;
		}
		getArtworks(table, sortQuery, tabIndex);
	}*/
	void addSortLogic (SortableLabel sl, int tabIndex, FlexTable table, Comparator<Artwork> cptr) {

		List<Artwork> list = artworksCache.get(tabIndex);

		if (!sl.isUsed()) {
			if (sl.toggle() == SortableLabel.SORT_ASCEND) {
				Collections.sort(list, cptr);			
			}
			else {
				Comparator<Artwork> cptr_reverse = Collections.reverseOrder(cptr);
				Collections.sort(list, cptr_reverse);
			}
		}
		else {
			sl.toggle();
			Collections.reverse(list);
		}
		
		displayArtworks(table, list, tabIndex);
	}

	//Integer rowIndexOfPriorSelectedCell = null;
	int[] rowIndexOfPriorSelectedCell = new int[NUMBER_OF_TABS];
	int[][] testing1 = new int[NUMBER_OF_TABS][2];

	//List<Artwork> artworksCache = new ArrayList<>();
	String[] queryCache = new String [NUMBER_OF_TABS];
	//ArrayList<Artwork>[] quersy = new ArrayList<Artwork>[NUMBER_OF_TABS];
	//ArrayList<String>[] subsection = new ArrayList<String>[4];
	// http://stackoverflow.com/a/1005083
	ArrayList<List<Artwork>> artworksCache;
	//List<Artwork>[] artworksCache = new List<Artwork>[3];
	

	void getArtworks(final FlexTable table, final String query, final int tabIndex) {

		artworkService.getArtworks(query, new AsyncCallback<List<Artwork>>() {

			public void onFailure(Throwable error) {
				handleError(error);
			}

			public void onSuccess(final List<Artwork> artworks) {
				//artworksCache = artworks;
				//queryCache = query;
				//GwtEvent<ClickHandler> asdf = new GwtEvent<ClickHandler>();
				/*table.getWidget(0, 0).fireEvent(new GwtEvent<ClickHandler>() {

					@Override
					public com.google.gwt.event.shared.GwtEvent.Type<ClickHandler> getAssociatedType() {
						// TODO Auto-generated method stub
						//return null;
						ClickEvent.fireNativeEvent(nativeEvent, handlerSource);
					}

					@Override
					protected void dispatch(ClickHandler handler) {
						// TODO Auto-generated method stub
						
					}
				});*/
				//ClickEvent.fireNativeEvent(nativeEvent, handlerSource);
				//NativeEvent focusEvent = Document.get().createFocusEvent();
				//ClickEvent.fireNativeEvent(focusEvent, table);

				
				
				table.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {

						try {
							Cell cell = table.getCellForEvent(event);

							if (cell != null) {
								HTMLTable.RowFormatter rf = table.getRowFormatter();

								String rowTableSelect = "rowSelect";
								if (rowIndexOfPriorSelectedCell != null)
									//rf.removeStyleName(rowIndexOfPriorSelectedCell, rowTableSelect);
									rf.removeStyleName(rowIndexOfPriorSelectedCell[tabIndex], rowTableSelect);

								int ri = cell.getRowIndex();
								rowIndexOfPriorSelectedCell[tabIndex] = ri;

								rf.addStyleName(ri, rowTableSelect);

								int artworkIndex = ri;
								Artwork artwork = artworks.get(artworkIndex);
								final LatLng artLatLong = LatLng.newInstance(artwork.coordinates.latitude, artwork.coordinates.longitude);
								//map.addOverlay(new Marker(ArtLatLong));

								int ci = cell.getCellIndex();
								//TODO: Need to get map to change upon clicking the button. Currently map is created locally in buildUI(), trying to build as part of the java file causes runtime errors.
								if (ci == PHOTO_COLUMN) {
									// https://groups.google.com/forum/#!topic/gwt-google-apis/LBs4W7eX4vc
									// use lock to prevent simultaneous calls
									synchronized(lock) {

										if (isLoading) {
											// then skip

										} 
										else {
											isLoading = true;
											
											if (directionResults != null)
												directionResults.clear();	
											
											loadDirections(artwork);										
										}
									}
									
									/*mainMap.setCenter(artLatLong);
									mainMap.panTo(artLatLong);
									mainMap.setZoomLevel(17);
<<<<<<< HEAD
									mainMap.getInfoWindow().open(mainMap.getCenter(),new InfoWindowContent(artwork.description));*/
=======
									mainMap.getInfoWindow().open(mainMap.getCenter(), infoWindowDisplay(artwork));
>>>>>>> 9294770e15051a66d09f290145b24f19251c416b
								}
									
							}
							else {
								HTMLTable.RowFormatter rf = table.getRowFormatter();

								String rowTableSelect = "rowSelect";
								if (rowIndexOfPriorSelectedCell != null)
									//rf.removeStyleName(rowIndexOfPriorSelectedCell, rowTableSelect);
									rf.removeStyleName(rowIndexOfPriorSelectedCell[tabIndex], rowTableSelect);

								//int ri = cell.getRowIndex();
								int ri = testing1[tabIndex][0];
								
								rowIndexOfPriorSelectedCell[tabIndex] = ri;
								
								int height = 225;
								scrollPanels[tabIndex].setVerticalScrollPosition(50+100*ri - height);

								rf.addStyleName(ri, rowTableSelect);

								if (ri < table.getRowCount()) {
									
									Artwork artwork = artworks.get(ri);
									
									// https://groups.google.com/forum/#!topic/gwt-google-apis/LBs4W7eX4vc
									/*if (haveDirections) {
										directionResults.clear();									
									}
									loadDirections(artwork);*/
								}
							}
						}
						catch (Exception e) {
							String message = e.getMessage();
						}
					}
				});
<<<<<<< HEAD
//			}, focusEvent);
				

				if (tabIndex == ARTWORKS_TAB)
					mainMap.clearOverlays();
				

=======
				
>>>>>>> 9294770e15051a66d09f290145b24f19251c416b
				displayArtworks(table, artworks, tabIndex);
				//displayArtworks2(artworks);
				//List<Artwork> adsf = artworksCache.get(tabIndex);
				artworksCache.set(tabIndex, artworks);
			}
		});
	}
	
	public InfoWindowContent infoWindowDisplay(final Artwork artwork) {
		final VerticalPanel ej = new VerticalPanel();
		final Button bb = new Button("More");
		ej.add(new HTML("<div style=\"line-height: normal; overflow: auto;\">" + "<p align= \"left\">" + "Name: " + artwork.title  + "<br />" + "Artist: " + artwork.artist  + "<br />" + "Year: " + artwork.yearInstalled  + "<br />" + artwork.description + "<br />" + "</p>" + "</div>"));
		ej.add(bb);
		//InfoWindowContent iwc = new InfoWindowContent("<div style=\"line-height: normal; overflow: auto;\">" + "<p align= \"left\">" + "Name: " + artwork.title  + "<br />" + "Artist: " + artwork.artist  + "<br />" + "Year: " + artwork.yearInstalled  + "<br />" + artwork.description + "<br />" + "</p>" + "</div>");
		InfoWindowContent iwc = new InfoWindowContent(ej);
		bb.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				popupDisplay(artwork);
				
			}
			
		});
		return iwc;
	}
	public void popupDisplay(Artwork artwork){	
		final PopUp db = new PopUp();
		final VerticalPanel vp = new VerticalPanel();
		final Button b = new Button("Close");
		db.setGlassEnabled(true);
		vp.add(new HTML("<p>" + "Name: " 
				+ artwork.title  + "<br />" + "Artist: " + artwork.artist  
				+ "<br />" + "Year: " + artwork.yearInstalled  + "</p>" + "<p>" + artwork.description + "</p>" + "<p>" + artwork.artistStatement + "</p>" + "<p>" + artwork.neighbourhood + "</p>" + "<p>" + artwork.siteName + "</p>" 
				+ "<p>" + artwork.address + "</p>" + "<p>" + artwork.locationOnSite + "</p>" + "</div>"));
		vp.add(b);
		db.add(vp);
		db.center();
		db.show();
		b.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				db.hide();				
				
			}
			
		});
		//return db;
		
	}

	Object lock = new Object();
	final int PHOTO_COLUMN = 0;


	// TODO: Dmitri --//
	private void displayArtworks(
			final FlexTable artworksTableInstance, final List<Artwork> artworks, final int tabIndex) {

		/*for (int i = 1; i < artworksTable.getRowCount(); i++)
			artworksTable.removeRow(i);		
		 */

		/*		for (Artwork artwork : artworks) {
			displayArtwork(artworksTableInstance, artwork);
		}*/


		//mainMap.clearOverlays();
		artworksTableInstance.removeAllRows();

		//final int  WORK_CHUNK = 5;

		// http://stackoverflow.com/a/16947034/3781601
		// use GWT scheduler to break up loading of table into increments so that UI remains reasonably responsive
		final int size = artworks.size();
		Scheduler.get().scheduleIncremental(new RepeatingCommand() {
			int rowCounter  = 0;

			@Override
			public boolean execute() {

				if (rowCounter >= size)
					return false;

				// do some work
				final Artwork artwork = artworks.get(rowCounter);
				displayArtwork(artworksTableInstance, artwork, tabIndex);

				rowCounter++;
				
				return true;
			}
			//	}, 200);
		});
		/*for (Artwork artwork : artworks) {
			displayArtwork(artworksTableInstance, artwork, tabIndex);
		}*/

		//if (tabIndex == )
	}


	//LatLng latLong;
	
	void addOverlays(Artwork artwork) {
		LatLng latLong = LatLng.newInstance(artwork.coordinates.latitude, artwork.coordinates.longitude);
	}
	
	//int rowIndex = -1;

	private void displayArtwork(final FlexTable table, final Artwork artwork, final int tabIndex) {

		final int ri = table.getRowCount();
		int ci = -1;  // column index
		table.setWidget(ri, ++ci, createImage(artwork.imgUrl, 100, 100));
		
		table.getFlexCellFormatter().setHeight(ri, 0, "100px");
		table.getFlexCellFormatter().setWidth(ri, 0, "100px");

		table.setText(ri, ++ci, artwork.title);
		//flexTable.getCellFormatter().addStyleName(ri,ci,"artworksTable-Cell");
		table.setText(ri, ++ci, artwork.artist);
		//flexTable.getCellFormatter().addStyleName(ri,ci,"artworksTable-Cell");
		table.setText(ri, ++ci, artwork.yearInstalled);
		//flexTable.getCellFormatter().addStyleName(ri,ci,"artworksTable-Cell");



		// format row colours
		// http://www.java2s.com/Code/Java/GWT/FlexTablewithrowstyle.htm
		HTMLTable.RowFormatter rf = table.getRowFormatter();
		// odd row
		if ((ri % 2) != 0) {
			rf.addStyleName(ri, "rowOdd");
		}
		// even row
		else {
			rf.addStyleName(ri, "rowEven");
		}


		/*Button removeStockButton = new Button("Map");
		final LatLng ArtLatLong = LatLng.newInstance(artwork.coordinates.latitude, artwork.coordinates.longitude);
		map.addOverlay(new Marker(ArtLatLong));
		removeStockButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//TODO: Need to get map to change upon clicking the button. Currently map is created locally in buildUI(), trying to build as part of the java file causes runtime errors.
				map.panTo(ArtLatLong);
				map.setZoomLevel(17);
				map.getInfoWindow().open(map.getCenter(),
						new InfoWindowContent(artwork.artistStatement));
			}});
		table.setWidget(ri, ++columnIndexLast, removeStockButton);*/
		LatLng latLong = LatLng.newInstance(artwork.coordinates.latitude, artwork.coordinates.longitude);

		final Marker marker = new Marker(latLong);
		mainMap.addOverlay(marker);
		marker.addMarkerClickHandler(new MarkerClickHandler() {
			
			@Override
			public void onClick(MarkerClickEvent event) {

				//InfoWindowContent iwc = new InfoWindowContent(artwork.description);

				mainMap.getInfoWindow().open(marker,infoWindowDisplay(artwork));				
				
				HTMLTable.RowFormatter rf = table.getRowFormatter();

				String rowTableSelect = "rowSelect";
				if (rowIndexOfPriorSelectedCell != null)
					rf.removeStyleName(rowIndexOfPriorSelectedCell[tabIndex], rowTableSelect);
				
				
				//DomEvent.fireNativeEvent(new ClickEvent() {}, table);
				rowIndexOfPriorSelectedCell[tabIndex] = ri;
				testing1[tabIndex][0] = ri;
				testing1[tabIndex][1] = 0;
				
				
				
				NativeEvent ne = Document.get().createClickEvent(0, 0, 0, 0, 0, false, false, false, false);

				DomEvent.fireNativeEvent(ne, table);
				 //evt.preventDefault();
			      //  final NativeEvent event = evt.getNativeEvent();

				
			       /* final NativeEvent newEvent = Document.get().createClickEvent(-1,
			            event.getScreenX(), event.getScreenY(), event.getClientX(), event.getClientY(),
			            event.getCtrlKey(), event.getAltKey(), event.getShiftKey(), event.getMetaKey());

			        DOM.eventGetTarget(Event.as(ne)).dispatchEvent(newEvent);
				table.fireEvent("click".);*/
				
//NativeEvent event2 = Document.get().createChangeEvent();

				//public final NativeEvent createClickEvent(int detail, int screenX, int screenY, int clientX, int clientY, boolean ctrlKey, boolean altKey, boolean shiftKey, boolean metaKey)
				
				//DomEvent.fireNativeEvent(Document.get().createChangeEvent(), themes);
				//DomEvent.fireNativeEvent(table.cre, themes);
					//System.out.println("");
				//rf.addStyleName(ri, rowTableSelect);
				//rowIndexOfPriorSelectedCell[tabIndex] = ri;
			}
		});
		

		//-- if user is logged in, then add voting buttons --//
		if (loginInfo.isLoggedIn()) {
			// http://www.gwtproject.org/javadoc/latest/com/google/gwt/user/client/ui/ToggleButton.html

			//final ToggleButton upvoteToggleButton = new ToggleButton("Upvote");
			//Image upvoteImage = new Image();
			//upvoteImage.setStyleName("upvoteImage");
			//upvotePressedImage.setStyleName("upvotePressedImage");
			//upvoteImage = createImage("images/arrow-up.png");
			//final ToggleButton upvoteToggleButton = new ToggleButton(upvoteImage);
			//Image uiu = new Image();
			//uiu.setStyleName("upvoteImageUnpressed");

			Image ua = createImage("images/up-arrow.png", 50, 50);
			Image uap = createImage("images/up-arrow-pressed.png", 50, 50);
			Image da = createImage("images/down-arrow.png", 50, 50);
			Image dap = createImage("images/down-arrow-pressed.png", 50, 50);

			final ToggleButton upvoteToggleButton = new ToggleButton(ua, uap);
			upvoteToggleButton.setStyleName("clearButton");
			final ToggleButton downvoteToggleButton = new ToggleButton(da, dap);
			downvoteToggleButton.setStyleName("clearButton");

			final int vote = artwork.ratings.getVote(loginInfo.userId);

			//-- add favourites button --//

			switch (vote) {
			case Ratings.UPVOTE:
				upvoteToggleButton.setDown(true);
				break;
			case Ratings.DOWNVOTE:
				downvoteToggleButton.setDown(true);
				break;
			default:
				// Do nothing
			}			

			final Label upvoteLabel = new Label();
			upvoteLabel.setStyleName("voteLabel");
			final Label downvoteLabel = new Label();
			downvoteLabel.setStyleName("voteLabel");

			// implement upvote button
			VerticalPanel upvoteVp = new VerticalPanel();
			upvoteLabel.setText(artwork.ratings.numberOfUpvotes.toString());
			upvoteVp.add(upvoteToggleButton);
			upvoteVp.add(upvoteLabel);
			table.setWidget(ri, ++ci, upvoteVp);

			upvoteToggleButton.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {

					Cell cell = table.getCellForEvent(event);
					
					if (cell != null) {
						int ri2 = cell.getRowIndex();
						
						if (upvoteToggleButton.isDown()) {
	
							artwork.ratings.addUserUpvote(loginInfo.userId);
							
							//displayArtwork(table, artwork, tabIndex);
							//artworksCache.get(FAVOURITES_TAB).add(ri2, artwork);
	
							if (downvoteToggleButton.isDown()) {
								downvoteToggleButton.setDown(false);
								downvoteLabel.setText(artwork.ratings.numberOfDownvotes.toString());
							}
						}
						// else upvoteToggleButton is up
						else {
							artwork.ratings.removeUserVote(loginInfo.userId);
							
							//tables[FAVOURITES_TAB].removeRow(ri2);
							//artworksCache.get(FAVOURITES_TAB).remove(ri2);						
						}
						
						//updateArtworkToDatabaseThenRefresh(artwork, FAVOURITES_TAB);
						//displayArtwork(tables[FAVOURITES_TAB], artwork, tabIndex);
						updateArtworkToDatabaseThenRefreshExcluding(artwork,tabIndex);
						//upvoteToggleButton.setEnabled(false);
						upvoteLabel.setText(artwork.ratings.numberOfUpvotes.toString());
						//refreshThemes(listOfThemes[FAVOURITES_TAB]);
						
						//refreshThemes(listOfThemes[FAVOURITES_TAB]);
					}
				}
			});

			// implement downvote button
			VerticalPanel downvoteVp = new VerticalPanel();
			downvoteLabel.setText(artwork.ratings.numberOfDownvotes.toString());
			downvoteVp.add(downvoteToggleButton);
			downvoteVp.add(downvoteLabel);
			table.setWidget(ri, ++ci, downvoteVp);

			downvoteToggleButton.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					
					Cell cell = table.getCellForEvent(event);
					
					if (downvoteToggleButton.isDown()) {

						artwork.ratings.addUserDownvote(loginInfo.userId);

						if (upvoteToggleButton.isDown()) {
							upvoteToggleButton.setDown(false);
							upvoteLabel.setText(artwork.ratings.numberOfUpvotes.toString());
						}
					}
					else { // then downToggleButton is up
						artwork.ratings.removeUserVote(loginInfo.userId);
					}
					
					//updateArtworkToDatabaseThenRefresh(artwork, FAVOURITES_TAB);	
					updateArtworkToDatabaseThenRefreshExcluding(artwork,tabIndex);
					//upvoteToggleButton.setEnabled(false);
					downvoteLabel.setText(artwork.ratings.numberOfDownvotes.toString());
					//int ri2 = cell.getRowIndex();
					//tables[FAVOURITES_TAB].removeRow(ri2);

					//refreshThemes(listOfThemes[FAVOURITES_TAB]);
					
					//refreshThemes(listOfThemes[FAVOURITES_TAB]);

				}
			});
			//-- --//
			
			//-- add visited checkbox --//
			//final CheckBox checkBox = new CheckBox("Visited");
			final CheckBox checkBox = new CheckBox();
			boolean hasVisited = artwork.visits.hasVisited(loginInfo.userId);
			//if (tabIndex == VISITED_TAB)
				checkBox.setValue(hasVisited);


				final Label visitsLabel = new Label();
				VerticalPanel visitsVerticalPanel = new VerticalPanel();
				visitsVerticalPanel.add(checkBox);
				visitsVerticalPanel.add(visitsLabel);
			table.setWidget(ri, ++ci, visitsVerticalPanel);
			visitsLabel.setText(artwork.visits.getNumberOfVisits().toString());

			// Hook up a handler to find out when it's clicked.
			checkBox.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					
					Cell cell = table.getCellForEvent(event);

					if (cell != null) {
						int ri2 = cell.getRowIndex();

						// http://www.gwtproject.org/javadoc/latest/com/google/gwt/user/client/ui/CheckBox.html
						//boolean isChecked = ((CheckBox) event.getSource()).getValue();
						boolean isChecked = checkBox.getValue();
						
						if (isChecked) {
							//boolean result = artwork.visits.markAsVisited(loginInfo.userId);

							/*if (tabIndex == ARTWORKS_TAB) {
								artwork.visits.markAsVisited(loginInfo.userId);
								updateArtworkToDatabase(artwork, VISITED_TAB);
								table.removeRow(ri2);
							}*/

							//tables[ARTWORKS_TAB].removeRow(ri2);
							
							artwork.visits.markAsVisited(loginInfo.userId);
						}
						// if is unchecked
						else {
							//artwork.visits.markAsUnvisited(loginInfo.userId);

							/*if (tabIndex == VISITED_TAB) {
								artwork.visits.markAsUnvisited(loginInfo.userId);
								updateArtworkToDatabaseThenRefresh(artwork, ARTWORKS_TAB);
									table.removeRow(ri2);
							}*/
							//tables[VISITED_TAB].removeRow(ri2);
							

							artwork.visits.markAsUnvisited(loginInfo.userId);
						}
						
						/*if (tabIndex == ARTWORKS_TAB) {
							tables[ARTWORKS_TAB].removeRow(ri2);
							//updateArtworkToDatabase(artwork);
							//refreshThemes(listOfThemes[VISITED_TAB]);						
						}
						else {

							tables[VISITED_TAB].removeRow(ri2);
							//updateArtworkToDatabase(artwork);
							//refreshThemes(listOfThemes[ARTWORKS_TAB]);
						}*/
						//updateArtworkToDatabase(artwork);
						

						updateArtworkToDatabaseThenRefreshExcluding(artwork,tabIndex);
						visitsLabel.setText(artwork.visits.getNumberOfVisits().toString());
					}
				}
			});
		}
		//-- --//
	}

	void updateArtworkToDatabase(Artwork artwork) {
		artworkService.saveArtwork(artwork, new AsyncCallback<Void>() {

			public void onSuccess(Void result) {
				// don't need to do anything
				for (ListBox lb : listOfThemes) {
					//refreshThemes(lb);
				}
			}

			public void onFailure(Throwable error) {
				handleError(error);
			}
		});
	}
	
	void updateArtworkToDatabaseThenRefresh(Artwork artwork, final int tabIndex) {
		artworkService.saveArtwork(artwork, new AsyncCallback<Void>() {

			public void onSuccess(Void result) {
				// don't need to do anything
				//refreshThemes(listOfThemes[tabIndex]);
			}

			public void onFailure(Throwable error) {
				handleError(error);
			}
		});
	}
	
	void updateArtworkToDatabaseThenRefreshExcluding(Artwork artwork, final int tabIndex) {
		artworkService.saveArtwork(artwork, new AsyncCallback<Void>() {

			public void onSuccess(Void result) {
				// don't need to do anything
				for (int i = 0; i < NUMBER_OF_TABS; i++) {
					if (i != tabIndex)
						refreshThemes(listOfThemes[i]);
				}
					
			}

			public void onFailure(Throwable error) {
				handleError(error);
			}
		});
	}

	Image createImage(String url, int width, int height){

		Image image = new Image();
		image.setUrl(url);
		image.setPixelSize(width, height);

		return image;
	}

	// -- --//

	private void handleError(Throwable error) {
		Window.alert(error.getMessage());
		if (error instanceof NotLoggedInException) {
			Window.Location.replace(loginInfo.getLogoutUrl());
		}
	}
}