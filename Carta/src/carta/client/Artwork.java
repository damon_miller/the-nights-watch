package carta.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;



import java.util.Set;

/*
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
*/
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

// "B. Encapsulate stock price data" 
// http://www.gwtproject.org/doc/latest/tutorial/codeclient.html

// Objectify
@Entity
//Implements serializable for GWT RPC serialization
//http://www.gwtproject.org/doc/latest/tutorial/RPC.html#serialize
public class Artwork implements Serializable {
	
	@Id Long id;
	
	// Objectify
	@Index public String title;	
	public Coordinates coordinates;
	public String urlLink;
	
	public String imgUrl;
	public String photoCredit;
	
	@Index public String artist;
	public String description;
	public String artistStatement;
	
	// location
	public String neighbourhood;
	public String siteName;
	public String address;
	public String locationOnSite;
	
	public String sourceProgram;
	@Index public String yearInstalled;
	public String primaryMaterials;
	public String type;
	public String status;
	public String ownership;
	public String sponsoringOrganization;
	public String linkToProjectPage;
	
	@Index public Ratings ratings = new Ratings();
	@Index public Views views = new Views();
	@Index public Visits visits = new Visits();
	
	void checkOffAsVisited() {
		
	}
	
	

	// need parameterless constructor for GWT RPC serialization
	// http://www.gwtproject.org/doc/latest/tutorial/RPC.html#serialize
	public Artwork() {
		
	}
}
