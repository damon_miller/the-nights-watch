package carta.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.junit.Test;

import carta.client.Artwork;
import carta.server.ArtworkKmlParser;
import carta.server.ArtworkKmlParser2;
import carta.server.ArtworkUrlParser;

public class ParserTests {

	ArtworkKmlParser Artkp = new ArtworkKmlParser();

	static final String urlPath = "http://data.vancouver.ca/download/kml/kml_public_art.zip";

	// ArtworkUrlParser Arturl = new ArtworkUrlParser();

	String s1 = "<![CDATA[Attributes: NAME:Brockton Point TITLE:Beaver Crest Pole URL_LINK:<a href='https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program='>Brockton Point</a>]]>";

	String s2 = "<![CDATA[Attributes: NAME:Brockton Point TITLE:Beaver' Crest Pole URL_LINK:<a href='https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program='>Brockton Point</a>]]>";

	@Test(expected = IOException.class)
	public void StreamTest() {
		URL url;

		InputStream uis = null;

		try {
			url = new URL(urlPath);
			uis = url.openStream();
			ZipInputStream zis_ZIP = new ZipInputStream(uis);

			assertNull(ArtworkKmlParser2.childZipInputStreamFromZipInputStream(
					null, zis_ZIP));
			ArtworkKmlParser2.childZipInputStreamFromZipInputStream(
					"public_art_individual_locations.kmz", null);
			assertNull(ArtworkKmlParser2.childZipInputStreamFromZipInputStream(
					null, null));

			fail();

		} catch (java.io.IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	@Test
	public void loadKmlTest() throws IOException {

		// load remote file into memory for reading
		URL url1;

		InputStream uis = null;
		try {
			url1 = new URL(urlPath);
			uis = url1.openStream();

			fail();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// load remote file as ZIP file
		ZipInputStream zis_ZIP = new ZipInputStream(uis);
		// find position of KMZ file in ZIP file

		ZipInputStream zis_KMZ = null;

		// load KMZ file as ZIP file
		zis_KMZ = new ZipInputStream(zis_KMZ);
		// find position of KML file in KMZ file
		ZipInputStream zis_KML;

		zis_KMZ = Artkp.childZipInputStreamFromZipInputStream(
				"public_art_individual_locations.kmz", zis_ZIP);
		zis_KML = Artkp.childZipInputStreamFromZipInputStream(
				"public_art_individual_locations.kml", zis_KMZ);

		org.jsoup.nodes.Document doc = Jsoup.parse(zis_KML, null, "",
				Parser.xmlParser());

		List<Element> listOfExtendedData = doc.select("ExtendedData");
		List<Element> listOfCoordinates = doc.select("coordinates");

		assertEquals(listOfExtendedData.size(), listOfCoordinates.size());

		try {
			if (listOfExtendedData.size() == listOfCoordinates.size()) {

				fail("Data size and coodinate size is not equal");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "static-access", "static-access", "static-access",
			"static-access", "static-access" })
	@Test
	public void parseTest() {

		String s1 = "<![CDATA[Attributes: NAME:Brockton Point TITLE:Beaver Crest Pole URL_LINK:<a href='https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program='>Brockton Point</a>]]>";
		String s1a = "https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program=";

		String s2 = "<![CDATA[Attributes: NAME:Brockton Point TITLE:Beaver' Crest Pole URL_LINK:<a href='https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program='>Brockton Point</a>]]>";

		// assertTrue(Artkp.stringInBetweenTwoStrings(s1, "'", "'") == s1a);

		// "https://app.vancouver.ca/PublicArt_Net/ArtworkDetails.aspx?ArtworkID=94&Neighbourhood=&Ownership=&Program=");

		// assertEquals(ArtworkKmlParser.parse(0).size(), 0);
		assertFalse(ArtworkKmlParser.parse(5).size() == 2);

		if (ArtworkKmlParser.parse().size() > 331) {

			assertEquals(ArtworkKmlParser.parse(331).size(), 331);
			assertEquals(ArtworkKmlParser.parse().size(), 331);

			assertFalse(ArtworkKmlParser.parse().size() < 331);

		}

		String s1String = ArtworkKmlParser2.stringInBetweenTwoStrings(s1, "'",
				"'");
		String s2String = ArtworkKmlParser2.stringInBetweenTwoStrings(s2, "'",
				"'");

		assertFalse(s1String == s1a);
		assertFalse(s2String == s1a);

		// ArtworkKmlParser2 artkp2 = new ArtworkKmlParser2(5);

		for (int i = 0; i < ArtworkKmlParser.parse().size(); i++) {

			Artwork a1 = ArtworkKmlParser.parse().get(i);

			Boolean det1 = ((a1.artist.equals("Nancy Chew")
					&& (a1.yearInstalled.equals("2003"))
					&& a1.primaryMaterials
							.equals("bronze figure, Rundle stone waterfall, bench and pavers, cast iron, stainless steel letters")
					&& (a1.title == "Sliding Edge") && (a1.linkToProjectPage
					.isEmpty())));

			Boolean det2 = ((a1.artist.contains("Kiyoshi Takahashi")
					&& (a1.yearInstalled.contains("1975"))
					&& a1.primaryMaterials.contains("marble")
					&& (a1.title == "Woman")
					&& (a1.linkToProjectPage.isEmpty()) && (a1.ownership == "City Of Vancouver")));

			Boolean det3 = ((a1.artist.contains("Elek Imredy")
					&& (a1.yearInstalled.contains("1970"))
					&& a1.primaryMaterials.contains("aluminium screen")
					&& (a1.title == "Untitled")
					&& (a1.linkToProjectPage.isEmpty()) && (a1.ownership == "private")));

			@SuppressWarnings("unused")
			ArtworkUrlParser Arturl1 = new ArtworkUrlParser(s1);

			if ((det1 && det2 && det3) == true) {

				assertTrue(true);

			}

			// assertTrue(a1.address == "500 Nicola Street");
			// a1.yearInstalled.equals(2003);
			// a1.linkToProjectPage.equals(null);

			assertFalse(a1.title.isEmpty());
			assertTrue(a1.urlLink.contains("https:"));

			assertTrue(a1.coordinates != null);

		}

	}

}
